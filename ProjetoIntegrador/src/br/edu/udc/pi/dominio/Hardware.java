package br.edu.udc.pi.dominio;

import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.pi.dominio.processo.Estado;
import br.edu.udc.pi.dominio.processo.Processo;

public class Hardware {
	// Paramentros de configura��es
	private int clockCPU = 100;
	private int clockEntradaSaidaUm = 5;
	private int clockEntradaSaidaDois = 5;
	private int clockEntradaSaidaTres = 5;
	private int instrucoesExecutadas = 0;
	private int instrucoesEntradaSaidaExecutadas = 0;

	public int getClockCPU() {
		return clockCPU;
	}

	public void setClockCPU(int clockCPU) {
		this.clockCPU = clockCPU;
	}

	public int getClockEntradaSaidaUm() {
		return clockEntradaSaidaUm;
	}

	public void setClockEntradaSaidaUm(int clockEntradaSaidaUm) {
		this.clockEntradaSaidaUm = clockEntradaSaidaUm;
	}

	public int getClockEntradaSaidaDois() {
		return clockEntradaSaidaDois;
	}

	public void setClockEntradaSaidaDois(int clockEntradaSaidaDois) {
		this.clockEntradaSaidaDois = clockEntradaSaidaDois;
	}

	public int getClockEntradaSaidaTres() {
		return clockEntradaSaidaTres;
	}

	public void setClockEntradaSaidaTres(int clockEntradaSaidaTres) {
		this.clockEntradaSaidaTres = clockEntradaSaidaTres;
	}

	// Executa somente instru��es de CPU
	public int executaProcesso(Processo processoAtual, int instrucoesPrioridade) {

		// Seta o n�mero de instru��es executadas do processo em 0
		int instrucoesExecutadasDoProcesso = 0;

		// Obtem o vetor de instru��es do processo
		Vetor<Integer> instrucoesPrograma = processoAtual.getContextoSoftware().getInstrucoesPrograma();

		do {

			// Se um clock j� tiver sido realizada
			// reseta o clock em 0
			if (this.instrucoesExecutadas == this.clockCPU) {
				this.instrucoesExecutadas = 0;
			}

			// Se o processo n�o tiver instru��es para serem executadas
			// encerra o loop
			if (instrucoesPrograma.tamanho() <= 0) {
				break;
			}

			// Seta o processo em execu��o
			processoAtual.getContextoHardware().setEstado(Estado.EXECUCAO);
			
			// Obt�m instru��o da primeira posi��o do vetor
			Integer obtemPrimeiraPosicao = instrucoesPrograma.obtem(0);

			// Verifica e executa se for instru��o do tipo CPU
			if (obtemPrimeiraPosicao.intValue() == 0) {
				instrucoesPrograma.remove(0);
			} else {
				break;
			}

			//Incrementa o n�mero de instru��es executadas e instru��es do processo executadas
			this.instrucoesExecutadas++;
			instrucoesExecutadasDoProcesso++;

		// Realiza o loop enquanto existirem instru��es no processo, enquanto n�o fizer um clock completo
		// e a prioridade do processo ainda tiver direito de uso do hardware
		} while (instrucoesPrograma.tamanho() != 0 && this.instrucoesExecutadas < clockCPU
				&& instrucoesExecutadasDoProcesso < instrucoesPrioridade);

		// Incrementa nas est�sticas do processos quantas vezes ele entrou no hardware
		// e o n�mero total de instru��es executadas do processo at� o momento
		processoAtual.getEstatistica().setQuantidadeEntradasNaCPU(1);

		processoAtual.getEstatistica().setQuantidadeInstrucoesCPUExecutadas(instrucoesExecutadasDoProcesso);

		// Retorna quantas instru��es do processo foram executadas
		return instrucoesExecutadasDoProcesso;
	}

	// Executa instru��es do tipo Entrada/Saida
	public int executaInstrucaoEntradaSaida(Processo processoEntradaSaida) {

		// Seta os valores de instru��es executadas e o clock
		// permitido em 0
		int clockEntradaSaida = 0;
		int instrucoesExecutadasDoProcesso = 0;

		Vetor<Integer> instrucoesEntradaSaida = processoEntradaSaida.getContextoSoftware().getInstrucoesPrograma();

		// Obtem o clock de Entrada/Saida referente ao tipo de instru��o 
		switch (instrucoesEntradaSaida.obtem(0).intValue()) {

		case 1:
			clockEntradaSaida = this.clockEntradaSaidaUm;
			break;

		case 2:
			clockEntradaSaida = this.clockEntradaSaidaDois;
			break;

		case 3:
			clockEntradaSaida = this.clockEntradaSaidaTres;
			break;
		}

		do {
			
			if (this.instrucoesEntradaSaidaExecutadas == clockEntradaSaida) {
				this.instrucoesEntradaSaidaExecutadas = 0;
			}

			Integer obtemPrimeiraPosicao = instrucoesEntradaSaida.obtem(0);
			
			// Seta o processo em execu��o
			processoEntradaSaida.getContextoHardware().setEstado(Estado.EXECUCAO);

			// Verifica e executa se n�o for uma instru��o de CPU
			if (obtemPrimeiraPosicao.intValue() != 0) {
				instrucoesEntradaSaida.remove(0);
			} else {
				break;
			}

			// Incrementa o total de instru��e executadas
			instrucoesEntradaSaidaExecutadas++;
			instrucoesExecutadasDoProcesso++;

		// Realiza o loop se ainda existirem instru��es no processo e o processo ainda possuir
		// direito de uso do hardware
		} while (instrucoesEntradaSaida.tamanho() != 0 && instrucoesEntradaSaidaExecutadas < clockEntradaSaida);

		// Incrementa nas estat�sticas do processo a quantidade de vezes que ele entrou no
		//hardware de Entrada/Sa�da e o n�mero de instru��es executadas do processo
		processoEntradaSaida.getEstatistica().setQuantidadeEntradasNaCPUEntradaSaida(1);

		processoEntradaSaida.getEstatistica()
				.setQuantidadeInstrucoesEntradaSaidaExecutadas(instrucoesExecutadasDoProcesso);

		// Retonra a quantidade de instru��es que ainda tem no processo
		return instrucoesEntradaSaidaExecutadas;
	}

}
