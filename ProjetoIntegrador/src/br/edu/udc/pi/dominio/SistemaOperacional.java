package br.edu.udc.pi.dominio;

import java.util.Random;

import javax.management.RuntimeErrorException;

import br.edu.udc.ed.lista.Lista;
import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.pi.dominio.processo.Estado;
import br.edu.udc.pi.dominio.processo.Prioridade;
import br.edu.udc.pi.dominio.processo.Processo;

public class SistemaOperacional {
	// Listas de prioridade
	final private Lista<Processo> processosProntosBaixaPrioridade = new Lista<>();
	final private Lista<Processo> processosProntosMediaPrioridade = new Lista<>();
	final private Lista<Processo> processosProntosAltaPrioridade = new Lista<>();

	// Listas de espera
	final private Lista<Processo> esperaEntradaSaidaUm = new Lista<>();
	final private Lista<Processo> esperaEntradaSaidaDois = new Lista<>();
	final private Lista<Processo> esperaEntradaSaidaTres = new Lista<>();
	final private Lista<Processo> esperaPausados = new Lista<>();

	// Lista de termino
	final private Lista<Processo> processosTerminados = new Lista<>();

	// Instancia Hardware
	final private Hardware hardware = new Hardware();
	
	// Estat�sticas das Listas
	final private EstatisticasLista estatisticasLista = new EstatisticasLista();

	public EstatisticasLista getEstatisticasLista() {
		return estatisticasLista;
	}
	
	public Hardware getHardware() {
		return hardware;
	}

	public Lista<Processo> getEsperaEntradaSaidaUm() {
		return esperaEntradaSaidaUm;
	}

	public Lista<Processo> getEsperaEntradaSaidaDois() {
		return esperaEntradaSaidaDois;
	}

	public Lista<Processo> getEsperaEntradaSaidaTres() {
		return esperaEntradaSaidaTres;
	}

	public Lista<Processo> getEsperaPausados() {
		return esperaPausados;
	}

	public Lista<Processo> getProcessosProntosBaixaPrioridade() {
		return processosProntosBaixaPrioridade;
	}

	public Lista<Processo> getProcessosProntosMediaPrioridade() {
		return processosProntosMediaPrioridade;
	}

	public Lista<Processo> getProcessosProntosAltaPrioridade() {
		return processosProntosAltaPrioridade;
	}

	public Lista<Processo> getProcessosTerminados() {
		return processosTerminados;
	}

	// V�lida instru��es do processo
	public boolean validaProcesso(Processo processo) {

		if (processo.getContextoSoftware().getInstrucoesCPU() == 0
				&& processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(0) == 0
				&& processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(1) == 0
				&& processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(2) == 0) {
			return false;
		}

		return true;
	}

	// Cria novo processo
	public Processo criaProcesso(Prioridade prioridade, int instrucoesCPU, int instrucoesESUm, int instrucoesESDois,
			int instrucoesESTres, int framesMemoria) {

		final Processo processo = new Processo();

		// Cria PID randomico baseado no hashcode do processo instanciado
		Random rand = new Random();
		final int processoID = rand.nextInt(processo.hashCode()) + 1;

		// Adiciona informa��es ao processo
		processo.getContextoSoftware().setpID(processoID);
		processo.getContextoSoftware().setPrioridade(prioridade);
		processo.getContextoSoftware().setFramesMemoria(framesMemoria);
		processo.getContextoSoftware().setInstrucoesCPU(instrucoesCPU);

		// Adiciona a quantidade de instru��es de entrada ao vetor instru��esEntradaSaida
		final Vetor<Integer> instrucoesEntradaSaida = new Vetor<>();
		instrucoesEntradaSaida.adiciona(instrucoesESUm);
		instrucoesEntradaSaida.adiciona(instrucoesESDois);
		instrucoesEntradaSaida.adiciona(instrucoesESTres);

		processo.getContextoSoftware().setInstrucoesEntradaSaida(instrucoesEntradaSaida);

		// Instancia vetor de instrul��es do processo
		final Vetor<Integer> instrucoesPrograma = new Vetor<>();

		// Preenche vetor com o n�mero de instru��es referentes a cada tipo
		for (int i = 0; i < instrucoesCPU; i++) {
			instrucoesPrograma.adiciona(0);
		}

		for (int i = 0; i < instrucoesESUm; i++) {
			instrucoesPrograma.adiciona(1);
		}

		for (int i = 0; i < instrucoesESDois; i++) {
			instrucoesPrograma.adiciona(2);
		}

		for (int i = 0; i < instrucoesESTres; i++) {
			instrucoesPrograma.adiciona(3);
		}

		// Embaralha a ordem de instru��es do vetor
		instrucoesPrograma.embaralha();

		// Adiciona vetor ao processo
		processo.getContextoSoftware().setInstrucoesPrograma(instrucoesPrograma);

		if (!this.validaProcesso(processo)) {
			throw new RuntimeErrorException(null, "Processo n�o pode ser nulo.");
		}

		// Se o processo for v�lido, ser� adicionado a fila de pronto referente a sua prioridade
		adicionaProcessoListaPorPrioridade(processo);

		return processo;
	}

	// Pausa processo recebendo a lista em que esta e sua posi��o nela
	public void pausarProcesso(Lista<Processo> lista, int posicao) {

		// Recupera o processo
		Processo processo = lista.consulta(posicao);

		// Verifica se o processo n�o esta terminado ou pausado
		if (processo.getContextoHardware().getEstado() == Estado.TERMINADO
				|| processo.getContextoHardware().getEstado() == Estado.PAUSADO) {
			throw new IndexOutOfBoundsException("Processo inv�lido.");
		}

		// Remove o processo da lista de origem
		processo = lista.remove(posicao);

		// Seta seu estado para pausado
		processo.getContextoHardware().setEstado(Estado.PAUSADO);

		// Insere na lista de pausados
		this.esperaPausados.insere(processo);
	}

	// Mata processo recebendo a lista em que esta e sua posi��o nela
	public void matarProcesso(Lista<Processo> lista, int posicao) {

		// Recupera o processo
		Processo processo = lista.consulta(posicao);

		// Verifica se o processo n�o esta terminado
		if (processo.getContextoHardware().getEstado() == Estado.TERMINADO) {
			throw new IndexOutOfBoundsException("Processo inv�lido.");
		}

		// Remove o processo da lista de origem
		processo = lista.remove(posicao);

		// Seta seu estado para terminado
		processo.getContextoHardware().setEstado(Estado.TERMINADO);

		// Insere na lista de terminados
		this.processosTerminados.insere(processo);
	}

	// Continua processo recebendo a lista em que esta e sua posi��o nela
	public void continuarProcesso(Lista<Processo> lista, int posicao) {

		// Recupera o processo
		Processo processo = lista.consulta(posicao);

		// Verifica se o processo existe, est� pausado e a lista recebida � a de pausados
		if (processo.getContextoHardware().getEstado() != Estado.PAUSADO || !lista.equals(this.getEsperaPausados())) {
			throw new IndexOutOfBoundsException("Processo pausado inv�lido.");
		}

		// Remove o processo da lista de origem
		processo = lista.remove(posicao);

		// Seta seu estado para pronto
		processo.getContextoHardware().setEstado(Estado.PRONTO);

		// Insere na lista de pronto referente a sua prioridade
		this.adicionaProcessoListaPorPrioridade(processo);

	}

	// Gerencia toda a utiliza��o do hardware
	public void gerenciaExecucaoProcessos() {
		
		// Tempo de CPU
		final int tempoCPU = hardware.getClockCPU();
		final int tempoClockEntradaSaidaUm = hardware.getClockEntradaSaidaUm();
		final int tempoClockEntradaSaidaDois = hardware.getClockEntradaSaidaDois();
		final int tempoClockEntradaSaidaTres = hardware.getClockEntradaSaidaTres();
		final int tempoCPUPrioridadeAlta = (int) (tempoCPU * (60 / 100.0f));
		final int tempoCPUPrioridadeMedia = (int) (tempoCPU * (30 / 100.0f));
		final int tempoCPUPrioridadeBaixa = (int) (tempoCPU * (10 / 100.0f));

		// Valores referentes ao uso da CPU
		int instrucoesExecutadasPelaCPU = 0;
		int instrucoesExecutadasDoProcesso = 0;
		int numeroInstrucoesExecutadasPrioridadeAlta = tempoCPUPrioridadeAlta;
		int numeroInstrucoesExecutadasPrioridadeMedia = tempoCPUPrioridadeMedia;
		int numeroInstrucoesExecutadasPrioridadeBaixa = tempoCPUPrioridadeBaixa;

		do {

			// Instancia um processoAtual em nulo
			Processo processoAtual = null;

			// Verifica se existem processos nas listas de prontos
			// para serem executados
			if (tamanhoListaProntos() > 0) {

				// Verifica se os processos de prioridade baixa ainda tem direito
				// a uma parte das instru��es e a lista est� vazia
				if (numeroInstrucoesExecutadasPrioridadeBaixa != 0 && this.processosProntosBaixaPrioridade.tamanho() == 0) {

					// Se sobrarem instru��es, passa para os processos de prioridade alta
					numeroInstrucoesExecutadasPrioridadeAlta = numeroInstrucoesExecutadasPrioridadeAlta
							+ numeroInstrucoesExecutadasPrioridadeBaixa;
					numeroInstrucoesExecutadasPrioridadeBaixa = 0;

				}

				// Verifica se os processos de prioridade alta ainda tem direito
				// a uma parte das instru��es e a lista est� vazia
				if (numeroInstrucoesExecutadasPrioridadeAlta != 0 && this.processosProntosAltaPrioridade.tamanho() == 0) {

					// Se sobrarem instru��es, passa para os processos de prioridade m�dia
					numeroInstrucoesExecutadasPrioridadeMedia = numeroInstrucoesExecutadasPrioridadeMedia
							+ numeroInstrucoesExecutadasPrioridadeAlta;
					numeroInstrucoesExecutadasPrioridadeAlta = 0;

				}

				// Verifica se os processos de prioridade m�dia ainda tem direito
				// a uma parte das instru��es e a lista est� vazia
				if (numeroInstrucoesExecutadasPrioridadeMedia != 0 && this.processosProntosMediaPrioridade.tamanho() == 0) {

					// Se sobrarem instru��es, passa para os processos de prioridade baixa
					numeroInstrucoesExecutadasPrioridadeBaixa = numeroInstrucoesExecutadasPrioridadeBaixa
							+ numeroInstrucoesExecutadasPrioridadeMedia;
					numeroInstrucoesExecutadasPrioridadeMedia = 0;

				}

				// Verifica se os processos de prioridade alta ainda tem direito
				// ao uso da CPU e se existem processos na lista
				if (numeroInstrucoesExecutadasPrioridadeAlta != 0 && this.processosProntosAltaPrioridade.tamanho() != 0) {

					// Remove o processo da fila
					processoAtual = this.processosProntosAltaPrioridade.remove();
					// O hardware executa o processo e retorna o n�mero de instru��es
					// que foram executadas
					instrucoesExecutadasDoProcesso = this.hardware.executaProcesso(processoAtual,
							numeroInstrucoesExecutadasPrioridadeAlta);
					// O n�mero de instru��es que a prioridade alta tem direito � atualizado
					// subtraindo o que ela j� executou
					numeroInstrucoesExecutadasPrioridadeAlta = numeroInstrucoesExecutadasPrioridadeAlta
							- instrucoesExecutadasDoProcesso;
					// Atualiza o total de instru��es que o hardware j� executou independete da prioridade
					instrucoesExecutadasPelaCPU = instrucoesExecutadasPelaCPU + instrucoesExecutadasDoProcesso;

				// Verifica se os processos de prioridade m�dia ainda tem direito
				// ao uso da CPU e se existem processos na lista
				} else if (numeroInstrucoesExecutadasPrioridadeMedia != 0
						&& this.processosProntosMediaPrioridade.tamanho() != 0) {

					// Remove o processo da fila
					processoAtual = this.processosProntosMediaPrioridade.remove();
					// O hardware executa o processo e retorna o n�mero de instru��es
					// que foram executadas
					instrucoesExecutadasDoProcesso = this.hardware.executaProcesso(processoAtual,
							numeroInstrucoesExecutadasPrioridadeMedia);
					// O n�mero de instru��es que a prioridade m�dia tem direito � atualizado
					// subtraindo o que ela j� executou
					numeroInstrucoesExecutadasPrioridadeMedia = numeroInstrucoesExecutadasPrioridadeMedia
							- instrucoesExecutadasDoProcesso;
					// Atualiza o total de instru��es que o hardware j� executou independete da prioridade
					instrucoesExecutadasPelaCPU = instrucoesExecutadasPelaCPU + instrucoesExecutadasDoProcesso;

				// Verifica se os processos de prioridade baixa ainda tem direito
				// ao uso da CPU e se existem processos na lista
				} else if (numeroInstrucoesExecutadasPrioridadeBaixa != 0
						&& this.processosProntosBaixaPrioridade.tamanho() != 0) {

					// Remove o processo da fila
					processoAtual = this.processosProntosBaixaPrioridade.remove();
					// O hardware executa o processo e retorna o n�mero de instru��es
					// que foram executadas
					instrucoesExecutadasDoProcesso = this.hardware.executaProcesso(processoAtual,
							numeroInstrucoesExecutadasPrioridadeBaixa);
					// O n�mero de instru��es que a prioridade baixa tem direito � atualizado
					// subtraindo o que ela j� executou
					numeroInstrucoesExecutadasPrioridadeBaixa = numeroInstrucoesExecutadasPrioridadeBaixa
							- instrucoesExecutadasDoProcesso;
					// Atualiza o total de instru��es que o hardware j� executou independete da prioridade
					instrucoesExecutadasPelaCPU = instrucoesExecutadasPelaCPU + instrucoesExecutadasDoProcesso;

				}

				// Se o processo retornado pela CPU n�o for nulo
				// move pra lista de acordo com as instru��es restantes
				if (processoAtual != null) {
					moverProcessoPorTipoInstrucao(processoAtual);
				}
				
				// Se o n�mero de instru��es for igual ao m�ximo de instru��es
				// permitidas por clock termina o clock
				if (instrucoesExecutadasPelaCPU == tempoCPU) {
					break;
				}

			} else {
				
				Processo processoEntradaSaida = null;
				int tipoInstrucaoEntradaSaida = 0;

				// Verifica se existem processos em espera nas tr�s listas
				if (this.esperaEntradaSaidaUm.tamanho() > 0) {
					processoEntradaSaida = this.esperaEntradaSaidaUm.remove();
					tipoInstrucaoEntradaSaida = 1;
				} else if (this.esperaEntradaSaidaDois.tamanho() > 0) {
					processoEntradaSaida = this.esperaEntradaSaidaDois.remove();
					tipoInstrucaoEntradaSaida = 2;
				} else if (this.esperaEntradaSaidaTres.tamanho() > 0) {
					processoEntradaSaida = this.esperaEntradaSaidaTres.remove();
					tipoInstrucaoEntradaSaida = 3;
				}

				// Se tiver retornado algum processo
				if (processoEntradaSaida != null) {

					// Executa as instru��es em outro m�todo do hardware
					// e retorna se ainda existem instro��es a serem executadas naquele processo
					int instrucoesExecutadas = this.hardware.executaInstrucaoEntradaSaida(processoEntradaSaida);

					// Se n�o existem mais instru��es o processo � terminado
					// se n�o volta para a lista de pronto referente a sua prioridade
					if (processoEntradaSaida.getContextoSoftware().getInstrucoesPrograma().tamanho() > 0) {
						moverProcessoPorTipoInstrucao(processoEntradaSaida);
					} else {
						adicionaProcessoListaPorPrioridade(processoEntradaSaida);
					}
					
					// Se o n�mero de instru��es for igual ao m�ximo de instru��es
					// permitidas por clock de entrada e saida termina o clock
					if(tipoInstrucaoEntradaSaida == 1 && instrucoesExecutadas == tempoClockEntradaSaidaUm) {
						break;
					}else if(tipoInstrucaoEntradaSaida == 2 && instrucoesExecutadas == tempoClockEntradaSaidaDois) {
						break;
					} else if(tipoInstrucaoEntradaSaida == 3 && instrucoesExecutadas == tempoClockEntradaSaidaTres) {
						break;
					}

				}

			}

		// Realiza o loop at� n�o existem mais processos prontos ou em espera
		} while (tamanhoListaProntos() != 0 || tamanhoListaEspera() != 0);

	}

	public void adicionaProcessoListaPorPrioridade(Processo processo) {
		
		processo.getContextoHardware().setEstado(Estado.PRONTO);

		// Adiciona processo a lista de prontos conforme sua prioridade
		switch (processo.getContextoSoftware().getPrioridade()) {

		case BAIXA:
			this.processosProntosBaixaPrioridade.insere(processo);
			break;

		case MEDIA:
			this.processosProntosMediaPrioridade.insere(processo);
			break;

		case ALTA:
			this.processosProntosAltaPrioridade.insere(processo);
			break;

		}

	}

	public void moverProcessoPorTipoInstrucao(Processo processo) {

		//Recebe um processo e verifica o tipo da sua primeira instru��o
		//move para a lista referente a instru��o
		Vetor<Integer> instrucoesPrograma = processo.getContextoSoftware().getInstrucoesPrograma();
		
		// Se o processo ainda tiver instru��es
		if (instrucoesPrograma.tamanho() != 0) {

			Integer obtemPrimeiraPosicao = instrucoesPrograma.obtem(0);

			switch (obtemPrimeiraPosicao.intValue()) {

			case 0:
				processo.getContextoHardware().setEstado(Estado.PRONTO);
				adicionaProcessoListaPorPrioridade(processo);
				break;

			case 1:
				processo.getContextoHardware().setEstado(Estado.ESPERA);
				this.esperaEntradaSaidaUm.insere(processo);
				break;

			case 2:
				processo.getContextoHardware().setEstado(Estado.ESPERA);
				this.esperaEntradaSaidaDois.insere(processo);
				break;

			case 3:
				processo.getContextoHardware().setEstado(Estado.ESPERA);
				this.esperaEntradaSaidaTres.insere(processo);
				break;

			}

		} else {
			
			// se n�o move para a lista de terminados
			processo.getContextoHardware().setEstado(Estado.TERMINADO);
			this.processosTerminados.insere(processo);

		}

	}

	// Retorna o total do tamanho das 3 filas de prontos
	public int tamanhoListaProntos() {
		final int tamanhoListaProntos = this.processosProntosAltaPrioridade.tamanho()
				+ this.processosProntosMediaPrioridade.tamanho() + this.processosProntosBaixaPrioridade.tamanho();
		return tamanhoListaProntos;
	}
	
	// Retorna o total do tamanho das 3 filas de espera
	public int tamanhoListaEspera() {
		final int tamanhoListaEspera = this.esperaEntradaSaidaUm.tamanho() + this.esperaEntradaSaidaDois.tamanho()
				+ this.esperaEntradaSaidaTres.tamanho();
		return tamanhoListaEspera;
	}

}
