package br.edu.udc.pi.dominio;

import br.edu.udc.ed.lista.Lista;
import br.edu.udc.pi.dominio.processo.Processo;

public class EstatisticasLista {

	public float calculaMediaCPUPorEntradaLista(Lista<Processo> lista) {

		int tamanhoLista = lista.tamanho();
		float mediaInstrucoesCPUPorEntrada = 0;

		if (tamanhoLista > 0) {

			for (int i = 0; i < tamanhoLista; i++) {
				mediaInstrucoesCPUPorEntrada = mediaInstrucoesCPUPorEntrada
						+ lista.consulta(i).getEstatistica().calculaMediaInstrucoesCPUPorEntrada();
			}

			return mediaInstrucoesCPUPorEntrada;
		}

		return 0;
	}

	public float calculaMediaEntradaSaidaPorEntradaLista(Lista<Processo> lista) {

		int tamanhoLista = lista.tamanho();
		float mediaInstrucoesEntradaSaidaPorEntrada = 0;

		if (tamanhoLista > 0) {

			for (int i = 0; i < tamanhoLista; i++) {
				mediaInstrucoesEntradaSaidaPorEntrada = mediaInstrucoesEntradaSaidaPorEntrada
						+ lista.consulta(i).getEstatistica().calculaMediaInstrucoesEntradaSaidaPorEntrada();
			}

			return mediaInstrucoesEntradaSaidaPorEntrada;
		}

		return 0;
	}

}
