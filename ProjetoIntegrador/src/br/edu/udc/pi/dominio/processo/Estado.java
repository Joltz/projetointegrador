package br.edu.udc.pi.dominio.processo;

public enum Estado {
	PRONTO, EXECUCAO, ESPERA, TERMINADO, PAUSADO;
}
