package br.edu.udc.pi.dominio.processo;

public class Estatisticas {

	private int quantidadeEntradasNaCPU = 0;
	private int quantidadeInstrucoesCPUExecutadas = 0;
	private int quantidadeInstrucoesEntradaSaidaExecutadas = 0;
	private int quantidadeEntradasNaCPUEntradaSaida = 0;

	public int getQuantidadeEntradasNaCPU() {
		return quantidadeEntradasNaCPU;
	}

	public void setQuantidadeEntradasNaCPU(int quantidadeEntradasNaCPU) {
		this.quantidadeEntradasNaCPU = this.quantidadeEntradasNaCPU + quantidadeEntradasNaCPU;
	}

	public int getQuantidadeInstrucoesCPUExecutadas() {
		return quantidadeInstrucoesCPUExecutadas;
	}

	public void setQuantidadeInstrucoesCPUExecutadas(int quantidadeInstrucoesCPUExecutadas) {
		this.quantidadeInstrucoesCPUExecutadas = this.quantidadeInstrucoesCPUExecutadas
				+ quantidadeInstrucoesCPUExecutadas;
	}

	public int getQuantidadeInstrucoesEntradaSaidaExecutadas() {
		return quantidadeInstrucoesEntradaSaidaExecutadas;
	}

	public void setQuantidadeInstrucoesEntradaSaidaExecutadas(int quantidadeInstrucoesEntradaSaidaExecutadas) {
		this.quantidadeInstrucoesEntradaSaidaExecutadas = this.quantidadeInstrucoesEntradaSaidaExecutadas
				+ quantidadeInstrucoesEntradaSaidaExecutadas;
	}

	public int getQuantidadeEntradasNaCPUEntradaSaida() {
		return quantidadeEntradasNaCPUEntradaSaida;
	}

	public void setQuantidadeEntradasNaCPUEntradaSaida(int quantidadeEntradasNaCPUEntradaSaida) {
		this.quantidadeEntradasNaCPUEntradaSaida = this.quantidadeEntradasNaCPUEntradaSaida
				+ quantidadeEntradasNaCPUEntradaSaida;
	}

	public float calculaMediaInstrucoesCPUPorEntrada() {
		return ((float) this.quantidadeInstrucoesCPUExecutadas / (float) this.quantidadeEntradasNaCPU);
	}

	public float calculaMediaInstrucoesEntradaSaidaPorEntrada() {
		return ((float) this.quantidadeInstrucoesEntradaSaidaExecutadas / (float) this.quantidadeEntradasNaCPUEntradaSaida);
	}

	public float calculaMediaGeralPorEntrada() {
		return ((float) this.quantidadeInstrucoesEntradaSaidaExecutadas + (float) this.quantidadeInstrucoesCPUExecutadas)
				/ ((float) this.quantidadeEntradasNaCPU + (float) this.quantidadeEntradasNaCPUEntradaSaida);
	}
	
	public float calculaMediaInstrucoesCPUPorClock(int clockCPU) {
		return ((float) this.quantidadeInstrucoesCPUExecutadas / (float) clockCPU);
	}
	
	public float calculaMediaInstrucoesEntradaSaidaPorClock(int clocksEntradaSaida) {
		return ((float) this.quantidadeInstrucoesEntradaSaidaExecutadas / (float) clocksEntradaSaida);
	}

}
