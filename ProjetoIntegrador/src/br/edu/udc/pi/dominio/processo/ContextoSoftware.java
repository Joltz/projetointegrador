package br.edu.udc.pi.dominio.processo;

import br.edu.udc.ed.vetor.Vetor;

public class ContextoSoftware {

	private int pID;
	private Prioridade prioridade;
	private int instrucoesCPU;
	private Vetor<Integer> instrucoesEntradaSaida;
	private int framesMemoria;
	private Vetor<Integer> instrucoesPrograma;

	public int getpID() {
		return pID;
	}

	public void setpID(int pID) {
		this.pID = pID;
	}

	public Prioridade getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}

	public Vetor<Integer> getInstrucoesEntradaSaida() {
		return instrucoesEntradaSaida;
	}

	public void setInstrucoesEntradaSaida(Vetor<Integer> instrucoesEntradaSaida) {
		this.instrucoesEntradaSaida = instrucoesEntradaSaida;
	}

	public int getInstrucoesCPU() {
		return instrucoesCPU;
	}

	public void setInstrucoesCPU(int instrucoesCPU) {
		this.instrucoesCPU = instrucoesCPU;
	}

	public int getFramesMemoria() {
		return framesMemoria;
	}

	public void setFramesMemoria(int framesMemoria) {
		this.framesMemoria = framesMemoria;
	}

	public Vetor<Integer> getInstrucoesPrograma() {
		return instrucoesPrograma;
	}

	public void setInstrucoesPrograma(Vetor<Integer> instrucoesPrograma) {

		this.instrucoesPrograma = instrucoesPrograma;
	}

}
