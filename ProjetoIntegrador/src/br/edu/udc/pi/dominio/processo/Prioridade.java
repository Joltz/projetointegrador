package br.edu.udc.pi.dominio.processo;

public enum Prioridade {
	ALTA, MEDIA, BAIXA;
}
