package br.edu.udc.pi.dominio.processo;

public class Processo {

	final private ContextoSoftware contextoSoftware = new ContextoSoftware();
	final private ContextoHardware contextoHardware = new ContextoHardware();
	final private EspacoEnderecamento espacoEnderecamento = new EspacoEnderecamento();
	final private Estatisticas estatistica = new Estatisticas();

	public ContextoSoftware getContextoSoftware() {
		return contextoSoftware;
	}

	public ContextoHardware getContextoHardware() {
		return contextoHardware;
	}

	public EspacoEnderecamento getEspacoEnderecamento() {
		return espacoEnderecamento;
	}

	public Estatisticas getEstatistica() {
		return estatistica;
	}

	public boolean equal(Processo obj) {
		
		if (this.contextoSoftware.getpID() == obj.contextoSoftware.getpID()) {
			return true;
		}
		
		return false;
	}

	@Override
	public String toString() {
		return "Processo pID " + contextoSoftware.getpID();
	}

}
