package br.edu.udc.pi.dominio.processo;

public class ContextoHardware {
	
	private Estado estado;

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}
