package br.edu.udc.pi.dominio.testes;

import org.junit.Assert;
import org.junit.Test;

import br.edu.udc.ed.lista.Lista;
import br.edu.udc.ed.vetor.Vetor;
import br.edu.udc.pi.dominio.SistemaOperacional;
import br.edu.udc.pi.dominio.processo.Estado;
import br.edu.udc.pi.dominio.processo.Prioridade;
import br.edu.udc.pi.dominio.processo.Processo;

public class SistemaOperacionalTeste {

	@Test
	public void criaProcessoSemESPrioridadeBaixa() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);
		final Lista<Processo> baixaPrioridade = sistemaOperacional.getProcessosProntosBaixaPrioridade();

		Assert.assertNotNull(processo);
		Assert.assertNotNull(processo.getContextoSoftware().getpID());
		Assert.assertEquals(1, baixaPrioridade.tamanho());
	}

	@Test
	public void criaProcessoComUmaInstrucaoESPrioridadeBaixa() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 10;
		final int instrucoesESUm = 5;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);
		final Vetor<Integer> instrucoesESProcesso = processo.getContextoSoftware().getInstrucoesEntradaSaida();

		final int instrucaoESUm = instrucoesESProcesso.obtem(0);

		Assert.assertTrue(instrucoesESProcesso.contem(5));
		Assert.assertEquals(5, instrucaoESUm);
		Assert.assertNotNull(instrucoesESProcesso.obtem(0));
		Assert.assertNotNull(instrucoesESProcesso.obtem(1));
		Assert.assertNotNull(instrucoesESProcesso.obtem(2));
		Assert.assertEquals(3, instrucoesESProcesso.tamanho());
	}

	@Test
	public void criaProcessoComDuasInstrucoesESPrioridadeBaixa() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 5;
		final int instrucoesESDois = 7;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);
		final Vetor<Integer> instrucoesESProcesso = processo.getContextoSoftware().getInstrucoesEntradaSaida();

		final int instrucaoESUm = instrucoesESProcesso.obtem(0);
		final int instrucaoESDois = instrucoesESProcesso.obtem(1);

		Assert.assertTrue(instrucoesESProcesso.contem(5));
		Assert.assertEquals(5, instrucaoESUm);
		Assert.assertEquals(7, instrucaoESDois);
		Assert.assertNotNull(instrucoesESProcesso.obtem(0));
		Assert.assertNotNull(instrucoesESProcesso.obtem(1));
		Assert.assertNotNull(instrucoesESProcesso.obtem(2));
		Assert.assertEquals(3, instrucoesESProcesso.tamanho());
	}

	@Test
	public void criaProcessoComTresInstrucoesESPrioridadeBaixa() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final Integer instrucoesESUm = 5;
		final Integer instrucoesESDois = 7;
		final Integer instrucoesESTres = 10;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);
		final Vetor<Integer> instrucoesESProcesso = processo.getContextoSoftware().getInstrucoesEntradaSaida();

		final int instrucaoESUm = instrucoesESProcesso.obtem(0);
		final int instrucaoESDois = instrucoesESProcesso.obtem(1);
		final int instrucaoESTres = instrucoesESProcesso.obtem(2);

		Assert.assertTrue(instrucoesESProcesso.contem(5));
		Assert.assertEquals(5, instrucaoESUm);
		Assert.assertEquals(7, instrucaoESDois);
		Assert.assertEquals(10, instrucaoESTres);
		Assert.assertNotNull(instrucoesESProcesso.obtem(0));
		Assert.assertNotNull(instrucoesESProcesso.obtem(1));
		Assert.assertNotNull(instrucoesESProcesso.obtem(2));
		Assert.assertEquals(3, instrucoesESProcesso.tamanho());
	}
	
	@Test(expected=AssertionError.class)
	public void criaProcessoSemInstrucoesDeveFalhar() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		Assert.fail("O processo n�o pode ser nulo.");
	}

	@Test
	public void pausarProcessoProntoDevePassar() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		final Lista<Processo> lista = sistemaOperacional.getProcessosProntosBaixaPrioridade();

		sistemaOperacional.pausarProcesso(lista, 0);

		final Lista<Processo> esperaPausados = sistemaOperacional.getEsperaPausados();

		Assert.assertNotNull(esperaPausados);
		Assert.assertEquals(1, esperaPausados.tamanho());
		Assert.assertEquals(processo, esperaPausados.consulta(0));
		Assert.assertEquals(0, sistemaOperacional.getProcessosProntosBaixaPrioridade().tamanho());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void pausarProcessoTerminadoDeveFalhar() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		processo.getContextoHardware().setEstado(Estado.TERMINADO);

		final Lista<Processo> lista = sistemaOperacional.getProcessosTerminados();

		lista.insere(processo);

		sistemaOperacional.pausarProcesso(lista, 0);

		final Lista<Processo> esperaPausados = sistemaOperacional.getEsperaPausados();

		Assert.fail("O processo n�o pode estar completo para ser pausado.");
		Assert.assertEquals(0, esperaPausados.tamanho());
		Assert.assertEquals(1, lista.tamanho());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void pausarProcessoEmEsperaDeveFalhar() {
		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		processo.getContextoHardware().setEstado(Estado.PAUSADO);

		final Lista<Processo> lista = sistemaOperacional.getEsperaEntradaSaidaDois();

		lista.insere(processo);

		sistemaOperacional.pausarProcesso(lista, 0);

		final Lista<Processo> esperaPausados = sistemaOperacional.getEsperaPausados();

		Assert.fail("O processo n�o pode estar em espera para ser pausado.");
		Assert.assertEquals(0, esperaPausados.tamanho());
		Assert.assertEquals(1, lista.tamanho());
	}

	@Test
	public void matarProcessoProntoDevePassar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		processo.getContextoHardware().setEstado(Estado.PRONTO);

		Lista<Processo> lista = sistemaOperacional.getProcessosProntosBaixaPrioridade();

		sistemaOperacional.matarProcesso(lista, 0);

		final Lista<Processo> processosTerminados = sistemaOperacional.getProcessosTerminados();

		Assert.assertNotNull(processosTerminados);
		Assert.assertEquals(1, processosTerminados.tamanho());
		Assert.assertEquals(processo, processosTerminados.consulta(0));
		Assert.assertEquals(0, lista.tamanho());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void matarProcessoTerminadoDeveFalhar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		processo.getContextoHardware().setEstado(Estado.TERMINADO);

		Lista<Processo> lista = sistemaOperacional.getProcessosTerminados();

		lista.insere(processo);

		sistemaOperacional.matarProcesso(lista, 0);

		Assert.fail("O processo n�o pode estar completo para ser terminado.");
		Assert.assertEquals(1, lista.tamanho());
	}

	@Test
	public void continuarProcessoEmEsperaDevePassar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		Lista<Processo> listaProntos = sistemaOperacional.getProcessosProntosBaixaPrioridade();

		sistemaOperacional.pausarProcesso(listaProntos, 0);

		Lista<Processo> lista = sistemaOperacional.getEsperaPausados();

		sistemaOperacional.continuarProcesso(lista, 0);

		Assert.assertEquals(1, listaProntos.tamanho());
		Assert.assertEquals(processo, listaProntos.consulta(0));
		Assert.assertEquals(Estado.PRONTO, processo.getContextoHardware().getEstado());
		Assert.assertEquals(0, lista.tamanho());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void continuarProcessoProntoDeveFalhar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		Lista<Processo> listaProntos = sistemaOperacional.getProcessosProntosBaixaPrioridade();

		sistemaOperacional.continuarProcesso(listaProntos, 0);

		Assert.fail("O processo deve estar em espera para ser continuado.");
		Assert.assertEquals(processo, listaProntos.consulta(0));
		Assert.assertEquals(1, listaProntos.tamanho());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void continuarProcessoTerminadoDeveFalhar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 500;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.BAIXA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		Lista<Processo> listaTerminados = sistemaOperacional.getProcessosTerminados();

		listaTerminados.insere(processo);

		sistemaOperacional.continuarProcesso(listaTerminados, 0);

		Assert.fail("O processo deve estar em espera para ser continuado.");
		Assert.assertEquals(processo, listaTerminados.consulta(0));
		Assert.assertEquals(1, listaTerminados.tamanho());
	}

	@Test
	public void compartilhamentoDeTempoProcessosDuasPrioridadeAltaUmaMediaUmaBaixa() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.ALTA, 20, instrucoesESUm, instrucoesESDois,
				instrucoesESTres, framesMemoria);

		final Processo processoDois = sistemaOperacional.criaProcesso(Prioridade.ALTA, 60, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		final Processo processoTres = sistemaOperacional.criaProcesso(Prioridade.MEDIA, 30, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		final Processo processoQuatro = sistemaOperacional.criaProcesso(Prioridade.BAIXA, 30, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		sistemaOperacional.gerenciaExecucaoProcessos();

		Assert.assertEquals(0, processo.getContextoSoftware().getInstrucoesPrograma().tamanho());
		Assert.assertEquals(20, processoDois.getContextoSoftware().getInstrucoesPrograma().tamanho());
		Assert.assertEquals(0, processoTres.getContextoSoftware().getInstrucoesPrograma().tamanho());
		Assert.assertEquals(20, processoQuatro.getContextoSoftware().getInstrucoesPrograma().tamanho());
	}

	@Test
	public void numeroDeEntradasNaCPUEInstrucoesExecutadasDevePassar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.ALTA, 20, instrucoesESUm, instrucoesESDois,
				instrucoesESTres, framesMemoria);

		final Processo processoDois = sistemaOperacional.criaProcesso(Prioridade.ALTA, 60, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		final Processo processoTres = sistemaOperacional.criaProcesso(Prioridade.MEDIA, 120, instrucoesESUm,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		sistemaOperacional.criaProcesso(Prioridade.BAIXA, 30, instrucoesESUm, instrucoesESDois, instrucoesESTres,
				framesMemoria);

		sistemaOperacional.gerenciaExecucaoProcessos();
		sistemaOperacional.gerenciaExecucaoProcessos();
		sistemaOperacional.gerenciaExecucaoProcessos();

		final int entradasNaCPU = processo.getEstatistica().getQuantidadeEntradasNaCPU();
		final int quantidadeDeInstrucoesExecutadas = processo.getEstatistica().getQuantidadeInstrucoesCPUExecutadas();

		final int entradasNaCPUDois = processoDois.getEstatistica().getQuantidadeEntradasNaCPU();
		final int quantidadeDeInstrucoesExecutadasDois = processoDois.getEstatistica()
				.getQuantidadeInstrucoesCPUExecutadas();

		final int entradasNaCPUTres = processoTres.getEstatistica().getQuantidadeEntradasNaCPU();
		final int quantidadeDeInstrucoesExecutadasTres = processoTres.getEstatistica()
				.getQuantidadeInstrucoesCPUExecutadas();

		Assert.assertEquals(1, entradasNaCPU);
		Assert.assertEquals(20, quantidadeDeInstrucoesExecutadas);
		Assert.assertEquals(2, entradasNaCPUDois);
		Assert.assertEquals(60, quantidadeDeInstrucoesExecutadasDois);
		Assert.assertEquals(3, entradasNaCPUTres);
		Assert.assertEquals(120, quantidadeDeInstrucoesExecutadasTres);

	}

	@Test
	public void numeroDeEntradasNaCPUEInstrucoesExecutadasEnradaSaidaDevePassar() {

		final SistemaOperacional sistemaOperacional = new SistemaOperacional();

		final int instrucoesCPU = 0;
		final int instrucoesESUm = 0;
		final int instrucoesESDois = 0;
		final int instrucoesESTres = 0;
		final int framesMemoria = 5;

		final Processo processo = sistemaOperacional.criaProcesso(Prioridade.ALTA, instrucoesCPU, instrucoesESUm,
				instrucoesESDois, 11, framesMemoria);

		final Processo processoDois = sistemaOperacional.criaProcesso(Prioridade.ALTA, instrucoesCPU, 5,
				instrucoesESDois, instrucoesESTres, framesMemoria);

		sistemaOperacional.gerenciaExecucaoProcessos();
		sistemaOperacional.gerenciaExecucaoProcessos();
		sistemaOperacional.gerenciaExecucaoProcessos();

		final int entradasNaCPU = processo.getEstatistica().getQuantidadeEntradasNaCPUEntradaSaida();
		final int quantidadeDeInstrucoesExecutadas = processo.getEstatistica()
				.getQuantidadeInstrucoesEntradaSaidaExecutadas();

		final int entradasNaCPUDois = processoDois.getEstatistica().getQuantidadeEntradasNaCPUEntradaSaida();
		final int quantidadeDeInstrucoesExecutadasDois = processoDois.getEstatistica()
				.getQuantidadeInstrucoesEntradaSaidaExecutadas();

		Assert.assertEquals(2, entradasNaCPU);
		Assert.assertEquals(10, quantidadeDeInstrucoesExecutadas);
		Assert.assertEquals(1, entradasNaCPUDois);
		Assert.assertEquals(5, quantidadeDeInstrucoesExecutadasDois);

	}

}
