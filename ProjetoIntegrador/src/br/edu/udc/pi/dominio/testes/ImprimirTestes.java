package br.edu.udc.pi.dominio.testes;

import java.util.Scanner;

import br.edu.udc.ed.lista.Lista;
import br.edu.udc.pi.dominio.SistemaOperacional;
import br.edu.udc.pi.dominio.processo.Prioridade;
import br.edu.udc.pi.dominio.processo.Processo;

public class ImprimirTestes {

	public static void main(String[] args) {

		SistemaOperacional sistemaOperacional = new SistemaOperacional();

		Scanner scanner = new Scanner(System.in);

		menu(sistemaOperacional, scanner);

		scanner.close();

	}

	public static void menu(SistemaOperacional sistemaOperacional, Scanner scanner) {

		int opcao = -1;

		while (opcao != 0) {

			System.out.println("1 - Criar novo processo");
			System.out.println("2 - Pausar processo");
			System.out.println("3 - Eliminar processo");
			System.out.println("4 - Continuar processo");
			System.out.println("5 - Imprimir informa��es de um processo");
			System.out.println("6 - Imprimir estat�sticas de uma lista de processos");
			System.out.println("7 - Alterar configura��es do sistema");
			System.out.println("0 - Sair");

			opcao = scanner.nextInt();

			switch (opcao) {

			case 1:
				recebeInformacoesDoNovoProcesso(sistemaOperacional, scanner);
				executaProcessos(sistemaOperacional, scanner);
				break;

			case 2:
				gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
				break;

			case 3:
				gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
				break;

			case 4:
				gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
				break;

			case 5:
				gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
				break;

			case 6:
				gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
				break;

			case 7:
				imprimirConfiguracoesSistema(sistemaOperacional);
				gerenciaConfiguracaoSistema(sistemaOperacional, scanner);
				break;

			}

		}

	}

	public static void subMenu(int opcao, SistemaOperacional sistemaOperacional, Scanner scanner) {

		switch (opcao) {

		case 1:
			recebeInformacoesDoNovoProcesso(sistemaOperacional, scanner);
			break;

		case 2:
			gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
			break;

		case 3:
			gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
			break;

		case 4:
			gerenciaAcaoSobreProcessos(opcao, sistemaOperacional, scanner);
			break;

		case 5:
			break;

		default:
			System.out.println("Op��o inv�lida.");
			break;
		}

	}

	public static void recebeInformacoesDoNovoProcesso(SistemaOperacional sistemaOperacional, Scanner scanner) {

		System.out.println("Criar novo processo");
		System.out.printf("Prioridade: ");
		Prioridade prioridade = Prioridade.valueOf(scanner.next().toUpperCase());
		System.out.printf("N�mero de Instru��es de CPU: ");
		int instrucoesCPU = scanner.nextInt();
		System.out.printf("N�mero de Instru��es de E/S 1: ");
		int instrucoesEntradaSaidaUm = scanner.nextInt();
		System.out.printf("N�mero de Instru��es de E/S 2: ");
		int instrucoesEntradaSaidaDois = scanner.nextInt();
		System.out.printf("N�mero de Instru��es de E/S 3: ");
		int instrucoesEntradaSaidaTres = scanner.nextInt();
		System.out.printf("N�mero de Frames de Mem�ria: ");
		int framesMemoria = scanner.nextInt();

		sistemaOperacional.criaProcesso(prioridade, instrucoesCPU, instrucoesEntradaSaidaUm, instrucoesEntradaSaidaDois,
				instrucoesEntradaSaidaTres, framesMemoria);

	}

	public static void executaProcessos(SistemaOperacional sistemaOperacional, Scanner scanner) {

		while (sistemaOperacional.tamanhoListaEspera() != 0 || sistemaOperacional.tamanhoListaProntos() != 0) {

			sistemaOperacional.gerenciaExecucaoProcessos();

			System.out.println("Tamnho das Listas de Prontos " + sistemaOperacional.tamanhoListaProntos());
			System.out.println("Tamnho das Listas de Espera " + sistemaOperacional.tamanhoListaEspera());
			System.out.println("Tamnho das Listas de Pausados " + sistemaOperacional.getEsperaPausados().tamanho());
			System.out.println(
					"Tamnho das Listas de Terminados " + sistemaOperacional.getProcessosTerminados().tamanho());

			System.out.printf("\nDeseja criar, terminar, pausar ou continuar um processo?\n");
			System.out.println("1 - Criar | 2 - Pausar | 3 - Terminar | 4 - Continuar | 5 - Continuar Clock");
			int opcao = scanner.nextInt();
			subMenu(opcao, sistemaOperacional, scanner);

		}

	}

	public static void imprimeListaDeProntosEPausados(SistemaOperacional sistemaOperacional) {

		int tamanhoListaProntosAltaPrioridade = sistemaOperacional.getProcessosProntosAltaPrioridade().tamanho();

		System.out.printf("\n\nLista de Processos de Pronto Prioridade Alta: \n");

		for (int i = 0; i < tamanhoListaProntosAltaPrioridade; i++) {
			System.out.println(
					"Posicao " + i + " - " + sistemaOperacional.getProcessosProntosAltaPrioridade().consulta(i));
		}

		int tamanhoListaProntosMediaPrioridade = sistemaOperacional.getProcessosProntosMediaPrioridade().tamanho();

		System.out.printf("\n\nLista de Processos de Pronto Prioridade M�dia: \n");

		for (int i = 0; i < tamanhoListaProntosMediaPrioridade; i++) {
			System.out.println(
					"Posicao " + i + " - " + sistemaOperacional.getProcessosProntosMediaPrioridade().consulta(i));
		}

		int tamanhoListaProntosBaixaPrioridade = sistemaOperacional.getProcessosProntosBaixaPrioridade().tamanho();

		System.out.printf("\n\nLista de Processos de Pronto Prioridade Baixa: \n");

		for (int i = 0; i < tamanhoListaProntosBaixaPrioridade; i++) {
			System.out.println(
					"Posicao " + i + " - " + sistemaOperacional.getProcessosProntosBaixaPrioridade().consulta(i));
		}

		int tamanhoListaPausados = sistemaOperacional.getEsperaPausados().tamanho();

		System.out.printf("\n\nLista de Processos Pausados: \n");

		for (int i = 0; i < tamanhoListaPausados; i++) {
			System.out.println("Posi��o " + i + " - " + sistemaOperacional.getEsperaPausados().consulta(i));
		}

		int tamanhoListaEsperaEntradaSaidaUm = sistemaOperacional.getEsperaEntradaSaidaUm().tamanho();

		System.out.printf("\n\nLista de Processos em Espera Entrada/Sa�da 1: \n");

		for (int i = 0; i < tamanhoListaEsperaEntradaSaidaUm; i++) {
			System.out.println("Posi��o " + i + " - " + sistemaOperacional.getEsperaEntradaSaidaUm().consulta(i));
		}

		int tamanhoListaEsperaEntradaSaidaDois = sistemaOperacional.getEsperaEntradaSaidaDois().tamanho();

		System.out.printf("\n\nLista de Processos em Espera Entrada/Sa�da 2: \n");

		for (int i = 0; i < tamanhoListaEsperaEntradaSaidaDois; i++) {
			System.out.println("Posi��o " + i + " - " + sistemaOperacional.getEsperaEntradaSaidaDois().consulta(i));
		}

		int tamanhoListaEsperaEntradaSaidaTres = sistemaOperacional.getEsperaEntradaSaidaTres().tamanho();

		System.out.printf("\n\nLista de Processos em Espera Entrada/Sa�da 3: \n");

		for (int i = 0; i < tamanhoListaEsperaEntradaSaidaTres; i++) {
			System.out.println("Posi��o " + i + " - " + sistemaOperacional.getEsperaEntradaSaidaTres().consulta(i));
		}

		int tamanhoListaTerminados = sistemaOperacional.getProcessosTerminados().tamanho();

		System.out.printf("\n\nLista de Processos Terminados: \n");

		for (int i = 0; i < tamanhoListaTerminados; i++) {
			System.out.println("Posi��o " + i + " - " + sistemaOperacional.getProcessosTerminados().consulta(i));
		}

	}

	public static void gerenciaAcaoSobreProcessos(int opcaoDeAcao, SistemaOperacional sistemaOperacional,
			Scanner scanner) {

		int opcao = 0;

		imprimeListaDeProntosEPausados(sistemaOperacional);

		System.out.println("Deseja executar a a��o em qual lista?");
		System.out.println(
				"1 - Alta | 2 - Media | 3 - Baixa | 4 - Pausados | 5 - E/S 1 | 6 - E/S 2 | 7 - E/S 3 | 8 - Terminados");
		opcao = scanner.nextInt();

		Processo processo = null;
		Lista<Processo> lista = null;
		int posicao = -1;

		System.out.printf("\nEscolha a posi��o do processo que deseja executar uma a��o sobre: \n");

		if (opcao == 1) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getProcessosProntosAltaPrioridade().consulta(posicao);
			lista = sistemaOperacional.getProcessosProntosAltaPrioridade();
		} else if (opcao == 2) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getProcessosProntosMediaPrioridade().consulta(posicao);
			lista = sistemaOperacional.getProcessosProntosMediaPrioridade();
		} else if (opcao == 3) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getProcessosProntosBaixaPrioridade().consulta(posicao);
			lista = sistemaOperacional.getProcessosProntosBaixaPrioridade();
		} else if (opcao == 4) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getEsperaPausados().consulta(posicao);
			lista = sistemaOperacional.getEsperaPausados();
		} else if (opcao == 5) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getEsperaEntradaSaidaUm().consulta(posicao);
			lista = sistemaOperacional.getEsperaEntradaSaidaUm();
		} else if (opcao == 6) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getEsperaEntradaSaidaDois().consulta(posicao);
			lista = sistemaOperacional.getEsperaEntradaSaidaDois();
		} else if (opcao == 7) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getEsperaEntradaSaidaTres().consulta(posicao);
			lista = sistemaOperacional.getEsperaEntradaSaidaTres();
		} else if (opcao == 8) {
			posicao = scanner.nextInt();
			processo = sistemaOperacional.getProcessosTerminados().consulta(posicao);
			lista = sistemaOperacional.getProcessosTerminados();
		}
		
		

		if (processo != null) {

			switch (opcaoDeAcao) {

			case 2:
				sistemaOperacional.pausarProcesso(lista, posicao);
				break;

			case 3:
				sistemaOperacional.matarProcesso(lista, posicao);
				break;

			case 4:
				sistemaOperacional.continuarProcesso(lista, posicao);
				break;

			case 5:
				imprimirInformacoesProcesso(sistemaOperacional, lista, posicao);
				break;

			case 6:
				imprimirEstatisticasDeUmaLista(sistemaOperacional, lista);
				break;

			}

		} else {
			System.out.println("Processo inv�lido.");
		}

	}

	public static void imprimirInformacoesProcesso(SistemaOperacional sistemaOperacional, Lista<Processo> lista,
			int posicao) {

		Processo processo = lista.consulta(posicao);

		if (processo != null) {

			int totalClocksCPU = sistemaOperacional.getHardware().getClockCPU();

			int totalClocksEntradaSaida = sistemaOperacional.getHardware().getClockEntradaSaidaUm()
					+ sistemaOperacional.getHardware().getClockEntradaSaidaDois()
					+ sistemaOperacional.getHardware().getClockEntradaSaidaTres();

			System.out.println("PID: " + processo.getContextoSoftware().getpID());
			System.out.println("Estado: " + processo.getContextoHardware().getEstado());
			System.out.println("Prioridade: " + processo.getContextoSoftware().getPrioridade());
			System.out.println("Instru��es de CPU: " + processo.getContextoSoftware().getInstrucoesCPU());
			System.out.println(
					"Instru��es de E/S 1: " + processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(0));
			System.out.println(
					"Instru��es de E/S 2: " + processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(1));
			System.out.println(
					"Instru��es de E/S 3: " + processo.getContextoSoftware().getInstrucoesEntradaSaida().obtem(2));
			System.out.println("Frames de Memoria: " + processo.getContextoSoftware().getFramesMemoria());
			System.out.println("Instru��es de CPU Executadas: "
					+ processo.getEstatistica().getQuantidadeInstrucoesCPUExecutadas());
			System.out.println("Instru��es de Entrada/Saida Executadas: "
					+ processo.getEstatistica().getQuantidadeInstrucoesEntradaSaidaExecutadas());
			System.out.println("Entradas na CPU: " + processo.getEstatistica().getQuantidadeEntradasNaCPU());
			System.out.println("Entradas na CPU Entrada/Saida: "
					+ processo.getEstatistica().getQuantidadeEntradasNaCPUEntradaSaida());
			System.out.println("M�dia de Instru��es Executadas pelo N�mero de Clocks de CPU: "
					+ processo.getEstatistica().calculaMediaInstrucoesCPUPorClock(totalClocksCPU));
			System.out.println("M�dia de Instru��es Executadas pelo N�mero de Clocks de CPU Entrada/Sa�da: "
					+ processo.getEstatistica().calculaMediaInstrucoesEntradaSaidaPorClock(totalClocksEntradaSaida));
			System.out.println("M�dia de Instru��es Executadas pelo N�mero de Entradas na CPU: "
					+ processo.getEstatistica().calculaMediaInstrucoesCPUPorEntrada());
			System.out.println("M�dia de Instru��es Executadas pelo N�mero de Entradas na CPU Entrada/Saida: "
					+ processo.getEstatistica().calculaMediaInstrucoesEntradaSaidaPorEntrada());
			System.out.println("M�dia Geral de Instru��es Executadas por Entradas na CPU: "
					+ processo.getEstatistica().calculaMediaGeralPorEntrada());
			System.out.println();

		} else {
			System.out.println("Processo inv�lido.");
		}

	}

	public static void imprimirEstatisticasDeUmaLista(SistemaOperacional sistemaOperacional, Lista<Processo> lista) {

		System.out.println("M�dia de Instru��es Executadas pelo N�mero de Entradas na CPU: "
				+ sistemaOperacional.getEstatisticasLista().calculaMediaCPUPorEntradaLista(lista));
		System.out.println("M�dia de Instru��es Executadas pelo N�mero de Entradas na CPU Entrada/Sa�da: "
				+ sistemaOperacional.getEstatisticasLista().calculaMediaEntradaSaidaPorEntradaLista(lista));

	}

	public static void imprimirConfiguracoesSistema(SistemaOperacional sistemaOperacional) {

		System.out.println(
				"N�mero de Instru��es de CPU Executadas por Clock: " + sistemaOperacional.getHardware().getClockCPU());
		System.out.println("N�mero de Instru��es de Entrada/Sa�da 1 Executadas por Clock: "
				+ sistemaOperacional.getHardware().getClockEntradaSaidaUm());
		System.out.println("N�mero de Instru��es de Entrada/Sa�da 2 Executadas por Clock: "
				+ sistemaOperacional.getHardware().getClockEntradaSaidaDois());
		System.out.println("N�mero de Instru��es de Entrada/Sa�da 3 Executadas por Clock: "
				+ sistemaOperacional.getHardware().getClockEntradaSaidaTres());

	}

	public static void gerenciaConfiguracaoSistema(SistemaOperacional sistemaOperacional, Scanner scanner) {

		System.out.printf("\n\nNovo N�mero de Instru��es de CPU Executadas por Clock: ");
		sistemaOperacional.getHardware().setClockCPU(scanner.nextInt());
		;
		System.out.printf("Novo N�mero de Instru��es de Entrada/Sa�da 1 Executadas por Clock: ");
		sistemaOperacional.getHardware().setClockEntradaSaidaUm(scanner.nextInt());
		;
		System.out.printf("Novo N�mero de Instru��es de Entrada/Sa�da 2 Executadas por Clock: ");
		sistemaOperacional.getHardware().setClockEntradaSaidaDois(scanner.nextInt());
		;
		System.out.printf("Novo N�mero de Instru��es de Entrada/Sa�da 3 Executadas por Clock: ");
		sistemaOperacional.getHardware().setClockEntradaSaidaTres(scanner.nextInt());
		;

	}

}
