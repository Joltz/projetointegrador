package br.edu.udc.ed.lista;

public class Lista<T> {
	private class No {
		public No proximo;
		public T dado;

		public No(No proximo, T dado) {
			this.proximo = proximo;
			this.dado = dado;
		}

		public No(T dado) {
			this.proximo = null;
			this.dado = dado;
		}
	}

	private No inicio;
	private No fim;

	private int tamanho;

	public Lista() {
		inicio = null;
		fim = null;
		tamanho = 0;
	}

	public int tamanho() {
		return tamanho;
	}

	public void insere(T object) {
		No no;

		if (inicio == null)// primeiro elemento - lista vazia
		{
			no = new No(object);
			inicio = fim = no;

			// um acesso ao novo n�
		} else // j� existem elementos na lista
		{

			// inserir no final da fila
			no = new No(null, object);
			fim.proximo = no;
			fim = no;
			// um acesso ao novo n�
		}
		tamanho++;
	}

	public T remove() {
		return remove(0);
	}

	public T remove(int pos) {
		T obj;
		if (inicio == null)// se a lista est� vazia
			return null;
		if (inicio == fim)// se existe apenas um elemento na lista
		{
			obj = inicio.dado;
			inicio = fim = null;
			tamanho--;
			// um acesso ao n� inicio
			return obj;
		}
		if (pos <= 1)// remover o primeiro elemento da lista
		{
			obj = inicio.dado;
			inicio = inicio.proximo;
			// um acesso ao n� inicio
		} else if (pos >= tamanho)// remover o �ltimo elemento da lista
		{
			obj = fim.dado;
			fim.proximo = null;
			// um acesso ao n� fim
		} else // remover um elemento no meio da lista
		{
			No aux = inicio;
			while (pos > 1) {
				aux = aux.proximo;
				pos--;
				// um acesso ao n� aux
			}
			// remover o elemento aux
			obj = aux.proximo.dado;
			aux.proximo = aux.proximo.proximo;
		}
		tamanho--;
		return (T) obj;
	}

	public T consulta(int pos) {
		if (inicio == null)// se a lista est� vazia
			return null;
		if (inicio == fim)// se existe apenas um elemento na lista
		{
			return inicio.dado;
		}
		if (pos == 0)// consulta o primeiro elemento da lista
		{
			return inicio.dado;
		} else if (pos >= tamanho - 1)// consulta o �ltimo elemento da lista
		{
			return fim.dado;
		} else // consulta um elemento no meio da lista
		{
			No aux = inicio;
			while (pos > 0) {
				aux = aux.proximo;
				pos--;
			}
			// consulta o elemento aux
			return (T) aux.dado;
		}
	}
}
