package br.edu.udc.ed.vetor;

import java.util.Arrays;
import java.util.Random;

public class Vetor<T> {
	// Inicializando um array de Object com capacidade de 50
	private Object[] objects = new Object[50];

	private int quantidade = 0;

	public void adiciona(T object) {
		this.verificaCapacidade();
		this.objects[quantidade] = object;
		this.quantidade++;
	}

	public void adiciona(int posicao, T object) {
		if (!this.posicaoOcupada(posicao) && posicao != this.quantidade) {
			throw new IndexOutOfBoundsException("Posicao invalida.");
		}

		this.verificaCapacidade();
		// Desloca todos os objects para a direita a partir da posicao
		for (int i = this.quantidade - 1; i >= posicao; i -= 1) {
			this.objects[i + 1] = this.objects[i];
		}
		this.objects[posicao] = object;
		this.quantidade++;
	}

	@SuppressWarnings("unchecked")
	public T obtem(int posicao) {
		if (!this.posicaoOcupada(posicao)) {
			throw new IndexOutOfBoundsException("Posi��o invalida.");
		}
		return (T) this.objects[posicao];
	}

	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.quantidade;
	}

	public void remove(int posicao) {
		if (!this.posicaoOcupada(posicao)) {
			throw new IndexOutOfBoundsException("Posicao invalida.");
		}

		// Desloca os objects da direita para a esquerda
		for (int i = posicao; i < this.quantidade - 1; i++) {
			this.objects[i] = this.objects[i + 1];
		}

		this.quantidade--;
	}

	public boolean contem(T object) {
		for (int i = 0; i < this.quantidade; i++) {
			if (object.equals(this.objects[i])) {
				return true;
			}
		}
		return false;
	}

	public int tamanho() {
		return this.quantidade;
	}

	public String toString() {
		return Arrays.toString(objects);
	}

	private void verificaCapacidade() {
		// se ja estiver no maximo
		if (this.quantidade == this.objects.length) {
			final Object[] novaArray = new Object[this.objects.length * 2];

			// dobra capacidade
			for (int i = 0; i < this.objects.length; i++) {
				// copia os objects
				novaArray[i] = this.objects[i];
			}
			this.objects = novaArray;
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	public void embaralha() {
		//pega a quantidade de elementos no vetor
		int n = this.quantidade;
		
		//gera um numero inteiro aleatorio
		Random random = new Random();
		random.nextInt();
		
		for (int i = 0; i < n; i++) {
			int change = i + random.nextInt(n - i);
			T object = (T) this.objects[i];
			this.objects[i] = this.objects[change];
			this.objects[change] = object;
		}
		
	}

}
