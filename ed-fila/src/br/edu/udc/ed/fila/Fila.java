package br.edu.udc.ed.fila;

public class Fila<T> {
	private class No {
		public No proximo;
		public T dado;

		public No(No proximo, T dado) {
			this.proximo = proximo;
			this.dado = dado;
		}

		public No(T dado) {
			this.proximo = null;
			this.dado = dado;
		}
	}

	private No inicio;
	private No fim;

	private int tamanho;

	public Fila() {
		inicio = null;
		fim = null;
		tamanho = 0;
	}

	public int tamanho() {
		return tamanho;
	}

	public void insere(T object) {
		No no;

		if (inicio == null)// primeiro elemento - lista vazia
		{
			no = new No(object);
			inicio = fim = no;

			// um acesso ao novo n�
		} else // j� existem elementos na lista
		{ 
			
			// inserir no final da fila
			no = new No(null, object);
			fim.proximo = no;
			fim = no;
			// um acesso ao novo n�
		}
		tamanho++;
	}

	public T remove() {
		return remove(0);
	}

	public T remove(int pos) {
		T object;
		
		if (inicio == null)// se a fila est� vazia
			return null;
		
		if (inicio == fim)// se existe apenas um elemento na fila
		{
			object = inicio.dado;
			inicio = fim = null;
			tamanho--;
			return object;
		} else // remover o primeiro elemento da fila
		{
			object = inicio.dado;
			inicio = inicio.proximo;
		} 
		tamanho--;
		return object;
	}

	public T consulta(int pos) {
		if (inicio == null)// se a lista est� vazia
			return null;
		if (inicio == fim)// se existe apenas um elemento na lista
		{
			return inicio.dado;
		}
		if (pos == 0)// consulta o primeiro elemento da lista
		{
			return inicio.dado;
		} else if (pos >= tamanho - 1)// consulta o �ltimo elemento da lista
		{
			return fim.dado;
		} else // consulta um elemento no meio da lista
		{
			No aux = inicio;
			while (pos > 0) {
				aux = aux.proximo;
				pos--;
			}
			// consulta o elemento aux
			return aux.dado;
		}
	}
}
